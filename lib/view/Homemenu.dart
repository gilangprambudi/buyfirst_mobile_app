import 'package:buyfirst_mobile/model/Cart.dart';
import 'package:buyfirst_mobile/view/Component/AccountBallanceHeader.dart';
import 'package:buyfirst_mobile/view/Component/BuyFirstAppBar.dart';
import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'CartList.dart';
import 'Component/BuyFirstDrawer.dart';

//NearbyStoresModelClass

class NearbyStore {
  String image;
  String storeName;
  String location;
  String distance;
  String type;

  NearbyStore(
      {String image,
      String storeName,
      String location,
      String distance,
      String type}) {
    this.image = image;
    this.type = type;
    this.location = location;
    this.distance = distance;
    this.storeName = storeName;
  }
}

class Homemenu extends StatefulWidget {
  @override
  _HomemenuState createState() => _HomemenuState();
}

class _HomemenuState extends State<Homemenu> {
  Future _scanQR() async {
    try {
      BarcodeScanner.scan().then((qrValue) async{
        print(qrValue);
        CartHttpService cartHttp = new CartHttpService();
        SharedPreferences prefs = await SharedPreferences.getInstance();
        cartHttp.addCart(uid: prefs.getString('uid'), qty: 1).then((response) {
          print("CUHY");
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => ViewCart()));
        });
      });
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        print(ex.code);
      } else {
        print("Unknow Error $ex");
      }
    } on FormatException {
      print("Barcode Scanning Canceled");
    } catch (ex) {
      print("Unknow Error $ex");
    }
  }

  List<NearbyStore> nearbyStoresModel = [
    new NearbyStore(
        image: "assets/walmart_logo.png",
        storeName: "Walmart Kebon Jeruk",
        location: "Jl. Wahid Hasyim",
        distance: ".3 KM",
        type: "Minimarket"),
    new NearbyStore(
        image: "assets/walmart_logo.png",
        storeName: "Walmart Kebon Jeruk",
        location: "Jl. Wahid Hasyim",
        distance: ".3 KM",
        type: "Minimarket"),
    new NearbyStore(
        image: "assets/walmart_logo.png",
        storeName: "Walmart Kebon Jeruk",
        location: "Jl. Wahid Hasyim",
        distance: ".3 KM",
        type: "Minimarket"),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          primaryColor: Color(0xFF40C4FF),
          accentColor: Colors.white,
        ),
        home: Scaffold(
            drawer: new BuyFirstDrawer(),
            backgroundColor: Color(0xFFEBEBEB),
            appBar: new BuyFirstAppBar().myAppBar(),
            bottomNavigationBar: new Container(
              height: 50.0,
              color: Color(0xFF99E46B),
              child: RawMaterialButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ViewCart()));
                },
                child: Container(
                  padding: EdgeInsets.only(left: 30),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.shopping_cart, color: Colors.white),
                              SizedBox(width: 10),
                              Text(
                                "My Cart",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 22),
                              ),
                            ],
                          ),
                          flex: 1),
                      Expanded(
                          child: Text(
                            "4 Item(s) - Rp 13.000",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w700),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1)
                    ],
                  ),
                ),
              ),
            ),
            body: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  AccountBallanceHeader(),

                  //Mainmenu Button Card
                  Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      margin: EdgeInsets.all(20),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                //Menu Button #1 Pay
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.all(5),
                                      height: 50,
                                      width: 50,
                                      alignment: Alignment.center,
                                      child: RawMaterialButton(
                                        child: Container(
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Color(0xFF11AAEE)),
                                              borderRadius: BorderRadius.all(
                                                  Radius.elliptical(15, 15))),
                                          child: Icon(
                                            Icons.payment,
                                            size: 34,
                                            color: Color(0xFF11AAEE),
                                          ),
                                          height: 50,
                                          width: 50,
                                        ),
                                        onPressed: () {},
                                      ),
                                    ),
                                    Text("Pay",
                                        style:
                                            TextStyle(color: Color(0xFF11AAEE)),
                                        textAlign: TextAlign.center)
                                  ],
                                ),

                                SizedBox(width: 20),

                                //Menu Button #2 Scan
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.all(5),
                                      height: 70,
                                      width: 70,
                                      alignment: Alignment.center,
                                      child: RawMaterialButton(
                                        child: Container(
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  width: 2,
                                                  color: Color(0xFF11AAEE)),
                                              borderRadius: BorderRadius.all(
                                                  Radius.elliptical(15, 15))),
                                          child: Icon(
                                            Icons.center_focus_weak,
                                            size: 40,
                                            color: Color(0xFF11AAEE),
                                          ),
                                          height: 70,
                                          width: 70,
                                        ),
                                        onPressed: _scanQR,
                                      ),
                                    ),
                                    Text("Scan",
                                        style:
                                            TextStyle(color: Color(0xFF11AAEE)),
                                        textAlign: TextAlign.center)
                                  ],
                                ),

                                SizedBox(width: 20),

                                //Menu Button #3 Top Up
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.all(5),
                                      height: 50,
                                      width: 50,
                                      alignment: Alignment.center,
                                      child: RawMaterialButton(
                                        child: Container(
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Color(0xFF11AAEE)),
                                              borderRadius: BorderRadius.all(
                                                  Radius.elliptical(15, 15))),
                                          child: Icon(
                                            Icons.account_balance_wallet,
                                            size: 34,
                                            color: Color(0xFF11AAEE),
                                          ),
                                          height: 50,
                                          width: 50,
                                        ),
                                        onPressed: () {},
                                      ),
                                    ),
                                    Text("Top Up",
                                        style:
                                            TextStyle(color: Color(0xFF11AAEE)),
                                        textAlign: TextAlign.center)
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      )),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 30, left: 30, right: 45),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Row(children: <Widget>[
                              Icon(Icons.store, color: Color(0xFF777777)),
                              SizedBox(width: 10),
                              Text("Stores Nearby",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xFF777777))),
                            ])),
                        Expanded(
                            flex: 1,
                            child: Container(
                                alignment: Alignment.centerRight,
                                child: Text("3 Stores")))
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.all(20),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Column(
                        children: nearbyStoresModel
                            .map((data) => RawMaterialButton(
                                  onPressed: () {},
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 2,
                                          child: Image.asset(data.image),
                                        ),
                                        Expanded(
                                          flex: 4,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.stretch,
                                            children: <Widget>[
                                              Text(data.storeName,
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    color: Color(0xFF777777),
                                                    fontSize: 14,
                                                  )),
                                              SizedBox(height: 10),
                                              Text(data.location,
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      color: Color(0xFF777777),
                                                      fontSize: 12)),
                                            ],
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.stretch,
                                            children: <Widget>[
                                              Text(data.distance,
                                                  textAlign: TextAlign.right,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: Color(0xFF777777),
                                                      fontSize: 12)),
                                              SizedBox(height: 10),
                                              Text(data.type,
                                                  textAlign: TextAlign.right,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      color: Color(0xFF777777),
                                                      fontSize: 12)),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ))
                            .toList(),
                      ),
                    ),
                  )
                ],
              ),
            )));
  }
}
