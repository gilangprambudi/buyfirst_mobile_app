import 'package:flutter/material.dart';

class BuyFirstAppBar{

  AppBar myAppBar(){
    return AppBar(
      iconTheme: new IconThemeData(color: Colors.white),
      title: Row(
        children: <Widget>[
          Expanded(
              flex: 6,
              child: Padding(
                padding: EdgeInsets.only(left: 20),
                child: Image.asset(
                  "assets/Logo.png",
                  width: 50,
                  height: 30,
                ),
              )),
          Expanded(
              flex: 1,
              child: RawMaterialButton(
                child: Icon(
                  Icons.notifications,
                  color: Colors.white,
                ),
                onPressed: () {},
              ))
        ],
      ),
    );
  }

}
