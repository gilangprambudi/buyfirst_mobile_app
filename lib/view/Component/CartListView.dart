import 'package:buyfirst_mobile/class/auth_service.dart';
import 'package:buyfirst_mobile/model/Cart.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CartListView extends StatefulWidget {
  @override
  _CartListViewState createState() => _CartListViewState();
}

class _CartListViewState extends State<CartListView> {
  List<Cart> userCarts = [];
  CartHttpService cartHttpService = new CartHttpService();
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchDwellingCarts();
  }

  void fetchDwellingCarts() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    cartHttpService.getDwelledCartByUID(uid : prefs.getString('uid')).then((carts){
      print(carts[0].price);
      
      setState((){
        userCarts = carts;
      });
    
    });
  }


  @override
  Widget build(BuildContext context) {
    
    return ListView.builder(
      itemCount: userCarts.length,
      itemBuilder: (BuildContext ctxt, int index) {
        return Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
              child: new Row(
                children: <Widget>[
                  Image.network(userCarts[index].getProductImage(), width: 80),
                  Column(children: <Widget>[
                    Text(userCarts[index].product,
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Color(0xFF777777),
                            fontSize: 16)),
                    Text(userCarts[index].product+' Category',
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w300,
                            color: Color(0xFF777777)))
                  ], crossAxisAlignment: CrossAxisAlignment.start),
                  Expanded(
                    flex: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              InkWell(child: Text("+"), onTap: () {}),
                              Text(userCarts[0].qty.toString()),
                              InkWell(child: Text("-"), onTap: () {})
                            ]),

                        Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
                         InkWell(child: Icon(Icons.remove), onTap: (){},)
                        ],)
                      ],
                    ),
                  )
                ],
              ),
            ),
            Divider()
          ],
        );
      },
    );
  }
}
