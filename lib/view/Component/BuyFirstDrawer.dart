import 'package:buyfirst_mobile/class/auth_service.dart';
import 'package:buyfirst_mobile/class/firebase_authentication.dart';
import 'package:buyfirst_mobile/view/Homemenu.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../login.dart';
class BuyFirstDrawer extends StatefulWidget {
  @override
  _BuyFirstDrawerState createState() => _BuyFirstDrawerState();
}

class _BuyFirstDrawerState extends State<BuyFirstDrawer> {
  FirebaseAuthenticationServiceClass firebaseAuthService = new FirebaseAuthenticationServiceClass();
  FirebaseUser currentUser;
  String currentUserEmail = "";

  @override
  Widget build(BuildContext context) {
    

    firebaseAuthService.getCurrentUser().then((firebaseUser){
      setState((){this.currentUser = firebaseUser; this.currentUserEmail = firebaseUser.email;});  
    });  
    return Drawer(
        child: Column(
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(top: 40, left: 25, right: 20, bottom: 25),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(left: 10, right: 10),
                        child: Image.asset('assets/amy-grant.png')),
                    flex: 2),
                Expanded(
                    child: Column(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              margin:
                                  EdgeInsets.only(left: 10, bottom: 5, top: 5),
                              child: Text("Amy Grant",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Color(0xFF11AAEE),
                                      letterSpacing: 2,
                                      fontWeight: FontWeight.w500)),
                            )),
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              margin: EdgeInsets.only(left: 10, bottom: 10),
                              child: Text(currentUserEmail,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontSize: 10,
                                      color: Color(0xFF11AAEE),
                                      letterSpacing: 2,
                                      fontWeight: FontWeight.w300)),
                            )),
                      ],
                    ),
                    flex: 6),
              ],
            )),
        Expanded(
          child: ListView(
            children: <Widget>[
              ListTile(
                title: Text("Home"),
                leading: Icon(Icons.home),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Homemenu()));
                },
              ),
              ListTile(
                title: Text("Transfer"),
                leading: Icon(Icons.compare_arrows),
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(title: Text("Preference")),
              Ink(
                child: ListTile(
                    title: Text("Sign Out",
                        style: TextStyle(color: Colors.redAccent)),
                    onTap: () {
                      AuthService authService = new AuthService();
                      authService.signOut();
                      Navigator.push(
                          context,
                          CupertinoPageRoute(
                              builder: (context) => Scaffold(
                                      body: LoginView(
                                    primaryColor: Colors.blue,
                                    backgroundColor: Colors.white,
                                    backgroundImage:
                                        AssetImage("assets/bg.png"),
                                  ))));
                    }),
              ),
            ],
          ),
        )
      ],
    ));
  }
  
}
