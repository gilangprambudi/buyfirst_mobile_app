import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountBallanceHeader extends StatefulWidget {
  @override
  _AccountBallanceHeaderState createState() => _AccountBallanceHeaderState();
}

class _AccountBallanceHeaderState extends State<AccountBallanceHeader> {
  
  var accountUID;
  void getUID() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState((){
      accountUID = prefs.getString('uid');
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUID();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(boxShadow: <BoxShadow>[
        BoxShadow(
            color: Colors.black54, blurRadius: 15.0, offset: Offset(0.0, 0.75))
      ], color: Colors.white),
      child: Row(
        children: <Widget>[
          Expanded(
              flex: 1,
              child: Padding(
                child: Text("CURRENT BALANCE",
                    style: TextStyle(
                        color: Color(0xFF9B9B9B),
                        fontSize: 14,
                        letterSpacing: 2,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w500)),
                padding: EdgeInsets.all(20),
              )),
          Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: StreamBuilder<DocumentSnapshot>(
      stream: Firestore.instance.collection('users').document(accountUID).snapshots(),
      builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError)
          return new Text('Error: ${snapshot.error}');
        switch (snapshot.connectionState) {
          case ConnectionState.waiting: return new Text('Loading...');
          default:
            print(snapshot.data.data);
            return Align(child: Text('Rp ' + snapshot.data.data['balance'].toString(), style: TextStyle(fontSize: 20)), alignment: Alignment.centerRight);
        }
      },
    ),
              )),
        ],
      ),
    );
  }
}
