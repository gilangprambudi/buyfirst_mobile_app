import 'dart:convert';

import 'package:buyfirst_mobile/class/api_provider.dart';
import 'package:buyfirst_mobile/model/Cart.dart';
import 'package:buyfirst_mobile/view/Component/AccountBallanceHeader.dart';
import 'package:buyfirst_mobile/view/PaymentSuccess.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'Component/BuyFirstDrawer.dart';
import 'Component/CartListView.dart';

class ViewCart extends StatefulWidget {
  @override
  _ViewCartState createState() => _ViewCartState();
}

class _ViewCartState extends State<ViewCart> {
  int cartTotalPrice = 0;

  CartHttpService cartHttpService = new CartHttpService();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getTotalPrice();
  }

  void getTotalPrice() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    cartHttpService
        .getDwelledCartTotalPriceByUID(prefs.getString('uid'))
        .then((response) {
      setState(() {
        cartTotalPrice = response;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          primaryColor: Color(0xFF40C4FF),
          accentColor: Colors.white,
        ),
        home: Scaffold(
            drawer: new BuyFirstDrawer(),
            appBar: AppBar(
              iconTheme: new IconThemeData(color: Colors.white),
              title: Row(
                children: <Widget>[
                  Expanded(
                      flex: 6,
                      child: Padding(
                        padding: EdgeInsets.only(left: 20),
                        child: Image.asset(
                          "assets/Logo.png",
                          width: 50,
                          height: 30,
                        ),
                      )),
                  Expanded(
                      flex: 1,
                      child: RawMaterialButton(
                        child: Icon(
                          Icons.notifications,
                          color: Colors.white,
                        ),
                        onPressed: () {},
                      ))
                ],
              ),
            ),
            body: Container(
              color: Color(0xFFEBEBEB),
              child: Column(children: <Widget>[
                AccountBallanceHeader(),
                SizedBox(height: 10),
                Expanded(
                  flex: 4,
                  child: Card(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      margin: EdgeInsets.only(
                          top: 10, bottom: 10, left: 20, right: 20),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(child: CartListView()),
                      )),
                ),
                Expanded(
                  flex: 1,
                  child: Card(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      margin: EdgeInsets.only(
                          top: 10, bottom: 10, left: 20, right: 20),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                            child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                  flex: 3,
                                  child: Text("Total Price",
                                      style: TextStyle(fontSize: 18))),
                              Expanded(
                                  flex: 1,
                                  child:
                                      Text('Rp ' + cartTotalPrice.toString())),
                            ],
                          ),
                        )),
                      )),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 10, left: 20, right: 20, bottom: 10),
                    child: Container(
                        child: Column(children: <Widget>[
                      SizedBox(
                        child: RaisedButton(
                          color: Color(0xFF40C4FF),
                          textColor: Colors.white,
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 15, right: 15, top: 10, bottom: 10),
                              child: Text("Pay Now!",
                                  style: TextStyle(fontSize: 20))),
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(20.0)),
                          onPressed: () {
                            Alert(
                              context: context,
                              type: AlertType.info,
                              title: "Pay Cart",
                              desc: "Are you sure to pay for the transaction?",
                              buttons: [
                                DialogButton(
                                  child: Text(
                                    "Yes",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 20),
                                  ),
                                  onPressed: () async {
                                    SharedPreferences prefs =
                                        await SharedPreferences.getInstance();
                                    http.post(
                                        ApiProvider.baseUrl +
                                            '/create-transaction',
                                        body: {
                                          'uid': prefs.getString('uid'),
                                          'payment_method': 'card'
                                        }).then((response) {
                                      Navigator.pushReplacement(
                                          context,
                                          CupertinoPageRoute(
                                              builder: (context) => Scaffold(
                                                  body: PaymentSuccess(
                                                    response
                                                          .body.toString()))));
                                    });
                                  },
                                  width: 120,
                                )
                              ],
                            ).show();
                          },
                        ),
                        width: double.infinity,
                      )
                    ])),
                  ),
                )
              ]),
            )));
  }

}
