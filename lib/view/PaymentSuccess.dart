import 'package:barcode_generator/barcode_generator.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class PaymentSuccess extends StatefulWidget {
  String transaction_id;
  PaymentSuccess(String transaction_id){
    this.transaction_id = transaction_id;
  }
  @override
  _PaymentSuccessState createState() => _PaymentSuccessState();
}

class _PaymentSuccessState extends State<PaymentSuccess> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.only(top: 18.0, left: 8, right: 8),
      child: Column(children: <Widget>[
        Center(
            child: Text("Payment Done. Succesfuly",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.w500))),
        Center(
            child: Text("Scan this Barcode to Cashier",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400))),
        Center(
          child: QrImage(
            data: widget.transaction_id,
            version: QrVersions.auto,
            size: 200.0,
          ),
        )
      ]),
    ));
  }
}
