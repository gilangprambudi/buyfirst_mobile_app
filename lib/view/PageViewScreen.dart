import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login.dart';

class PageViewScreen extends StatelessWidget {
  PageViewScreen() {}

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: Scaffold(body: pageView(context)));
  }

  void checkToHasSeen() async{
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool("pagerHasBeenSeen", true);
  }

  PageView pageView(BuildContext context) {
    return PageView(children: <Widget>[
      Container(
        child: Column(
          children: <Widget>[
            Image.asset("assets/view_page_image1.png"),
            SizedBox(
              height: 30,
            ),
            Text(
              "Spend less time paying,\nfocus on just what you buy",
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.w600),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: EdgeInsets.all(30),
              child: Text(
                "The one app to pay them all, connect your bank accounts, fintechs and other. All payment in one app.",
                style: TextStyle(
                    color: Color(0xFF777777),
                    fontSize: 18,
                    fontWeight: FontWeight.w300),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
        padding: EdgeInsets.only(top: 100),
      ),
      Container(
        child: Column(
          children: <Widget>[
            Image.asset("assets/view_page_image2.png"),
            SizedBox(
              height: 30,
            ),
            Text(
              "Sharing can’t ever be this easy!",
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.w600),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: EdgeInsets.all(30),
              child: Text(
                "Memorizing account number is hard, why\ndon’t we help you? Share by phone number,\nBuyFirst ID or Email.",
                style: TextStyle(
                    color: Color(0xFF777777),
                    fontSize: 18,
                    fontWeight: FontWeight.w300),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
        padding: EdgeInsets.only(top: 100),
      ),
      Container(
        child: Column(
          children: <Widget>[
            Image.asset("assets/view_page_image3.png"),
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Text(
                "Join now, among with other millions of people!",
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.w600),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: EdgeInsets.all(30),
              child: Text(
                "Get started using this app, register now,\nit's only one tap away.",
                style: TextStyle(
                    color: Color(0xFF777777),
                    fontSize: 18,
                    fontWeight: FontWeight.w300),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 60),
            RaisedButton(
              color: Color(0xFF40C4FF),
              textColor: Colors.white,
              child: Padding(
                  padding:
                      EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 15),
                  child: Text("Get Started!", style: TextStyle(fontSize: 26))),
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(28.0)),
              onPressed: () {

                checkToHasSeen();
                Navigator.push(
                    context,
                    CupertinoPageRoute(
                        builder: (context) => Scaffold(
                                body: LoginView(
                              primaryColor: Colors.blue,
                              backgroundColor: Colors.white,
                              backgroundImage: AssetImage("assets/bg.png"),
                            ))));
              },
            )
          ],
        ),
        padding: EdgeInsets.only(top: 100),
      ),
    ]);
  }
}
