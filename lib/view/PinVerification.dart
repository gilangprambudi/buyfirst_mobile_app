import 'package:buyfirst_mobile/class/slide_right_route.dart';
import 'package:flutter/material.dart';

import 'PageViewScreen.dart';

class PinVerificationScreen extends StatefulWidget {
  bool _yuhu = false;
  @override
  _PinVerificationScreenState createState() => _PinVerificationScreenState();
}

class _PinVerificationScreenState extends State<PinVerificationScreen> {
  String pinTemp = "";

  void appendPinTemp(String pin) {
    if(pinTemp.length < 6){
      setState(() {
        pinTemp = pinTemp + pin;
      });
    }else{
      Navigator.push(context, SlideRightRoute(page: PageViewScreen()));
    }
  }

  void backspacePin() {

    if(pinTemp.length > 0){
      setState(() {
        pinTemp = pinTemp.substring(0, pinTemp.length - 1);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          backgroundColor: Color(0xFFEBEBEB),
          body: Container(
            padding: EdgeInsets.only(top: 40, bottom: 30, left: 25, right: 25),
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: new Container(
                      decoration: new BoxDecoration(
                          color: Color(0xFFFFFFFF),
                          borderRadius:
                              new BorderRadius.all(Radius.circular(20))),
                      child: Column(
                        children: <Widget>[
                          Expanded(
                              flex: 2,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 40.0, left: 30, right: 30),
                                child: Container(
                                  child: Column(children: <Widget>[
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(bottom: 25.0),
                                      child: Center(
                                          child: Text("Insert Your Pin",
                                              style: TextStyle(
                                                  fontSize: 24,
                                                  fontWeight: FontWeight.w600,
                                                  color: Color(0xFF777777)))),
                                    ),
                                    Row(children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Center(
                                          child: SizedBox(
                                            width: 20,
                                            height: 20,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: pinTemp.length < 1
                                                      ? null
                                                      : Color(0xFFFF40C4FF),
                                                  border: Border.all(
                                                      color:
                                                          Color(0xFFFF40C4FF)),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(20))),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Center(
                                          child: SizedBox(
                                            width: 20,
                                            height: 20,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: pinTemp.length < 2
                                                      ? null
                                                      : Color(0xFFFF40C4FF),
                                                  border: Border.all(
                                                      color:
                                                          Color(0xFFFF40C4FF)),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(20))),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Center(
                                          child: SizedBox(
                                            width: 20,
                                            height: 20,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: pinTemp.length < 3
                                                      ? null
                                                      : Color(0xFFFF40C4FF),
                                                  border: Border.all(
                                                      color:
                                                          Color(0xFFFF40C4FF)),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(20))),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Center(
                                          child: SizedBox(
                                            width: 20,
                                            height: 20,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: pinTemp.length < 4
                                                      ? null
                                                      : Color(0xFFFF40C4FF),
                                                  border: Border.all(
                                                      color:
                                                          Color(0xFFFF40C4FF)),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(20))),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Center(
                                          child: SizedBox(
                                            width: 20,
                                            height: 20,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: pinTemp.length < 5
                                                      ? null
                                                      : Color(0xFFFF40C4FF),
                                                  border: Border.all(
                                                      color:
                                                          Color(0xFFFF40C4FF)),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(20))),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Center(
                                          child: SizedBox(
                                            width: 20,
                                            height: 20,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: pinTemp.length < 6
                                                      ? null
                                                      : Color(0xFFFF40C4FF),
                                                  border: Border.all(
                                                      color:
                                                          Color(0xFFFF40C4FF)),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(20))),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ])
                                  ]),
                                ),
                              )),
                          Expanded(
                              flex: 5,
                              child: Column(children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                          flex: 1,
                                          child: Center(
                                              child: SizedBox(
                                            width: 50,
                                            height: 50,
                                            child: RawMaterialButton(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(50),
                                                    side: BorderSide(
                                                        color:
                                                            Color(0xFF40C4FF),
                                                        width: 2)),
                                                child: Text("1",
                                                    style: TextStyle(
                                                        fontSize: 22,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color:
                                                            Color(0xFF40C4FF))),
                                                onPressed: () {
                                                  appendPinTemp("1");
                                                  print(pinTemp);
                                                }),
                                          ))),
                                      Expanded(
                                          flex: 1,
                                          child: Center(
                                              child: SizedBox(
                                            width: 50,
                                            height: 50,
                                            child: RawMaterialButton(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(50),
                                                    side: BorderSide(
                                                        color:
                                                            Color(0xFF40C4FF),
                                                        width: 2)),
                                                child: Text("2",
                                                    style: TextStyle(
                                                        fontSize: 22,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color:
                                                            Color(0xFF40C4FF))),
                                                onPressed: () {
                                                  appendPinTemp("2");
                                                  print(pinTemp);
                                                }),
                                          ))),
                                      Expanded(
                                          flex: 1,
                                          child: Center(
                                              child: SizedBox(
                                            width: 50,
                                            height: 50,
                                            child: RawMaterialButton(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(50),
                                                    side: BorderSide(
                                                        color:
                                                            Color(0xFF40C4FF),
                                                        width: 2)),
                                                child: Text("3",
                                                    style: TextStyle(
                                                        fontSize: 22,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color:
                                                            Color(0xFF40C4FF))),
                                                onPressed: () {
                                                  appendPinTemp("3");
                                                  print(pinTemp);
                                                }),
                                          ))),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                          flex: 1,
                                          child: Center(
                                              child: SizedBox(
                                            width: 50,
                                            height: 50,
                                            child: RawMaterialButton(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(50),
                                                    side: BorderSide(
                                                        color:
                                                            Color(0xFF40C4FF),
                                                        width: 2)),
                                                child: Text("4",
                                                    style: TextStyle(
                                                        fontSize: 22,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color:
                                                            Color(0xFF40C4FF))),
                                                onPressed: () {
                                                  appendPinTemp("4");
                                                  print(pinTemp);
                                                }),
                                          ))),
                                      Expanded(
                                          flex: 1,
                                          child: Center(
                                              child: SizedBox(
                                            width: 50,
                                            height: 50,
                                            child: RawMaterialButton(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(50),
                                                    side: BorderSide(
                                                        color:
                                                            Color(0xFF40C4FF),
                                                        width: 2)),
                                                child: Text("5",
                                                    style: TextStyle(
                                                        fontSize: 22,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color:
                                                            Color(0xFF40C4FF))),
                                                onPressed: () {
                                                  appendPinTemp("5");
                                                  print(pinTemp);
                                                }),
                                          ))),
                                      Expanded(
                                          flex: 1,
                                          child: Center(
                                              child: SizedBox(
                                            width: 50,
                                            height: 50,
                                            child: RawMaterialButton(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(50),
                                                    side: BorderSide(
                                                        color:
                                                            Color(0xFF40C4FF),
                                                        width: 2)),
                                                child: Text("6",
                                                    style: TextStyle(
                                                        fontSize: 22,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color:
                                                            Color(0xFF40C4FF))),
                                                onPressed: () {
                                                  appendPinTemp("6");
                                                  print(pinTemp);
                                                }),
                                          ))),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                          flex: 1,
                                          child: Center(
                                              child: SizedBox(
                                            width: 50,
                                            height: 50,
                                            child: RawMaterialButton(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(50),
                                                    side: BorderSide(
                                                        color:
                                                            Color(0xFF40C4FF),
                                                        width: 2)),
                                                child: Text("7",
                                                    style: TextStyle(
                                                        fontSize: 22,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color:
                                                            Color(0xFF40C4FF))),
                                                onPressed: () {
                                                  appendPinTemp("7");
                                                  print(pinTemp);
                                                }),
                                          ))),
                                      Expanded(
                                          flex: 1,
                                          child: Center(
                                              child: SizedBox(
                                            width: 50,
                                            height: 50,
                                            child: RawMaterialButton(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(50),
                                                    side: BorderSide(
                                                        color:
                                                            Color(0xFF40C4FF),
                                                        width: 2)),
                                                child: Text("8",
                                                    style: TextStyle(
                                                        fontSize: 22,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color:
                                                            Color(0xFF40C4FF))),
                                                onPressed: () {
                                                  appendPinTemp("8");
                                                  print(pinTemp);
                                                }),
                                          ))),
                                      Expanded(
                                          flex: 1,
                                          child: Center(
                                              child: SizedBox(
                                            width: 50,
                                            height: 50,
                                            child: RawMaterialButton(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(50),
                                                    side: BorderSide(
                                                        color:
                                                            Color(0xFF40C4FF),
                                                        width: 2)),
                                                child: Text("9",
                                                    style: TextStyle(
                                                        fontSize: 22,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color:
                                                            Color(0xFF40C4FF))),
                                                onPressed: () {
                                                  appendPinTemp("9");
                                                  print(pinTemp);
                                                }),
                                          ))),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Container(),
                                      ),
                                      Expanded(
                                          flex: 1,
                                          child: Center(
                                              child: SizedBox(
                                            width: 50,
                                            height: 50,
                                            child: RawMaterialButton(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(50),
                                                    side: BorderSide(
                                                        color:
                                                            Color(0xFF40C4FF),
                                                        width: 2)),
                                                child: Text("0",
                                                    style: TextStyle(
                                                        fontSize: 22,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color:
                                                            Color(0xFF40C4FF))),
                                                onPressed: () {
                                                  appendPinTemp("0");
                                                  print(pinTemp);
                                                }),
                                          ))),
                                      Expanded(
                                          flex: 1,
                                          child: Center(
                                              child: SizedBox(
                                            width: 50,
                                            height: 50,
                                            child: RawMaterialButton(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(50),
                                                    side: BorderSide(
                                                        color:
                                                            Color(0xFFF27E7E),
                                                        width: 2)),
                                                child: Center(
                                                    child: Image.asset(
                                                  "assets/backspace_btn_icon.png",
                                                  width: 25,
                                                )),
                                                onPressed: () {
                                                  backspacePin();
                                                  print(pinTemp);
                                                }),
                                          ))),
                                    ],
                                  ),
                                ),
                              ])),
                          Expanded(
                            flex: 1,
                            child: Container(
                                child: InkWell(
                                    onTap: () {},
                                    child: Center(
                                        child: Text("Forgot Pin?",
                                            style: TextStyle(
                                                color: Color(0xFFF27E7E)))))),
                          )
                        ],
                      )),
                )
              ],
            ),
          )),
    );
  }
}
