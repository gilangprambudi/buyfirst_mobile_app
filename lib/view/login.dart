import 'package:buyfirst_mobile/class/auth_service.dart';
import 'package:buyfirst_mobile/class/firebase_authentication.dart';
import 'package:buyfirst_mobile/view/Homemenu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Register.dart';

class LoginView extends StatefulWidget {
  final Color primaryColor;
  final Color backgroundColor;
  final AssetImage backgroundImage;

  LoginView(
      {Key key, this.primaryColor, this.backgroundColor, this.backgroundImage});

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final emailTextController = TextEditingController();
  final passwordTextController = TextEditingController();
  AuthService authService = new AuthService();

  @override
  void initState() {
    super.initState();
    checkSignIn();
  }

  @override
  void dispose() {
    emailTextController.dispose();
    passwordTextController.dispose();
    super.dispose();
  }

  void checkSignIn() async {
    authService.checkSignIn().then((isSignedIn) {
      if (isSignedIn) {
        Navigator.push(
            context,
            CupertinoPageRoute(
                builder: (context) => Scaffold(body: Homemenu())));
      }
    });
  }

  void loginPost() async {
    final prefs = await SharedPreferences.getInstance();
    String email = emailTextController.text;
    String password = passwordTextController.text;
    authService.signIn(email: email, password: password).then((result) {
      if (result != null) {
        prefs.setBool("isLoggedIn", true);
        prefs.setString('savedEmail', result.user.email);
        prefs.setString('uid', result.user.uid);
        Navigator.push(
            context,
            CupertinoPageRoute(
                builder: (context) => Scaffold(body: Homemenu())));
      } else {
        Alert(
          context: context,
          type: AlertType.error,
          title: "Wrong Email or Password",
          desc:
              "The specified email or password is incorrect, please try again",
          buttons: [
            DialogButton(
              child: Text(
                "Ok",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () => Navigator.pop(context),
              width: 120,
            )
          ],
        ).show();
      }
    });
  }


  void signInWithGoogle() async {
    FirebaseAuthenticationServiceClass firebaseAuth =
        new FirebaseAuthenticationServiceClass();
    firebaseAuth.firebaseLoginGoogle().whenComplete(() {
      firebaseAuth.getCurrentUser().then((authResult) async {
        if (authResult.uid != null) {
          final prefs = await SharedPreferences.getInstance();
          prefs.setBool("isLoggedIn", true);
          prefs.setString('savedEmail', authResult.email);
          prefs.setString('uid', authResult.uid);

          Navigator.push(
              context,
              CupertinoPageRoute(
                  builder: (context) => Scaffold(body: Homemenu())));
        }
      });
    });

  }

  @override
  Widget build(BuildContext context) {
    return new Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          color: widget.backgroundColor,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              new ClipPath(
                clipper: MyClipper(),
                child: Container(
                  decoration: BoxDecoration(
                    image: new DecorationImage(
                      image: widget.backgroundImage,
                      fit: BoxFit.cover,
                    ),
                  ),
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(top: 40.0, bottom: 85.0),
                  child: Column(
                    children: <Widget>[
                      new Image.asset("assets/Logo.png", width: 150),
                      Container(
                          margin: EdgeInsets.only(top: 20),
                          child: Text(
                            "Your One Stop Payment Service",
                            style: TextStyle(
                                color: Color(0xFFFFFFFF),
                                fontSize: 18,
                                letterSpacing: 2,
                                fontStyle: FontStyle.italic),
                          )),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 40.0),
                child: Text(
                  "Email",
                  style: TextStyle(color: Colors.grey, fontSize: 16.0),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.5),
                    width: 1.0,
                  ),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                margin: const EdgeInsets.symmetric(
                    vertical: 10.0, horizontal: 20.0),
                child: Row(
                  children: <Widget>[
                    new Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 15.0),
                      child: Icon(
                        Icons.person_outline,
                        color: Colors.grey,
                      ),
                    ),
                    Container(
                      height: 30.0,
                      width: 1.0,
                      color: Colors.grey.withOpacity(0.5),
                      margin: const EdgeInsets.only(left: 00.0, right: 10.0),
                    ),
                    new Expanded(
                      child: TextField(
                        controller: emailTextController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Enter your email',
                          hintStyle: TextStyle(color: Colors.grey),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 40.0),
                child: Text(
                  "Password",
                  style: TextStyle(color: Colors.grey, fontSize: 16.0),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.5),
                    width: 1.0,
                  ),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                margin: const EdgeInsets.symmetric(
                    vertical: 10.0, horizontal: 20.0),
                child: Row(
                  children: <Widget>[
                    new Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 15.0),
                      child: Icon(
                        Icons.lock_open,
                        color: Colors.grey,
                      ),
                    ),
                    Container(
                      height: 30.0,
                      width: 1.0,
                      color: Colors.grey.withOpacity(0.5),
                      margin: const EdgeInsets.only(left: 00.0, right: 10.0),
                    ),
                    new Expanded(
                      child: TextField(
                        obscureText: true,
                        autocorrect: false,
                        controller: passwordTextController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Enter your password',
                          hintStyle: TextStyle(color: Colors.grey),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20.0),
                padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                child: new Row(
                  children: <Widget>[
                    new Expanded(
                      child: FlatButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        splashColor: widget.primaryColor,
                        color: Colors.blue,
                        child: new Row(
                          children: <Widget>[
                            new Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: Text(
                                "LOGIN",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            new Expanded(
                              child: Container(),
                            ),
                            new Transform.translate(
                              offset: Offset(15.0, 0.0),
                              child: new Container(
                                padding: const EdgeInsets.all(5.0),
                                child: FlatButton(
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(28.0)),
                                  splashColor: Colors.white,
                                  color: Colors.white,
                                  child: Icon(
                                    Icons.arrow_forward,
                                    color: widget.primaryColor,
                                  ),
                                  onPressed: () {
                                    loginPost();
                                  },
                                ),
                              ),
                            )
                          ],
                        ),
                        onPressed: () {
                          // fetchPost() async {
                          //   final response = await http
                          //       .get('http://gilangprambudi.ddns.net');

                          //   if (response.statusCode == 200) {
                          //     // If the call to the server was successful, parse the JSON.
                          //     print(response.body);
                          //   } else {
                          //     // If that call was not successful, throw an error.
                          //     throw Exception('Failed to load post');
                          //   }
                          // }

                          // fetchPost();

                          loginPost();
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20.0),
                padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                child: new Row(
                  children: <Widget>[
                    new Expanded(
                      child: FlatButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        splashColor: widget.primaryColor,
                        color: Colors.red,
                        child: new Row(
                          children: <Widget>[
                            new Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: Text(
                                "LOGIN WITH GOOGLE",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            new Expanded(
                              child: Container(),
                            ),
                            new Transform.translate(
                              offset: Offset(15.0, 0.0),
                              child: new Container(
                                padding: const EdgeInsets.all(5.0),
                                child: FlatButton(
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(28.0)),
                                  splashColor: Colors.white,
                                  color: Colors.white,
                                  child: Image.asset("assets/google-icon.png",
                                      width: 20),
                                  onPressed: () {
                                    signInWithGoogle();
                                  },
                                ),
                              ),
                            )
                          ],
                        ),
                        onPressed: () {
                          // fetchPost() async {
                          //   final response = await http
                          //       .get('http://gilangprambudi.ddns.net');

                          //   if (response.statusCode == 200) {
                          //     // If the call to the server was successful, parse the JSON.
                          //     print(response.body);
                          //   } else {
                          //     // If that call was not successful, throw an error.
                          //     throw Exception('Failed to load post');
                          //   }
                          // }

                          // fetchPost();

                          signInWithGoogle();
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20.0),
                padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                child: new Row(
                  children: <Widget>[
                    new Expanded(
                      child: FlatButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        color: Colors.transparent,
                        child: Container(
                          padding: const EdgeInsets.only(left: 20.0),
                          alignment: Alignment.center,
                          child: Text(
                            "DON'T HAVE AN ACCOUNT?",
                            style: TextStyle(color: widget.primaryColor),
                          ),
                        ),
                        onPressed: () {
                          Navigator.push(
                              context,
                              CupertinoPageRoute(
                                  builder: (context) => Scaffold(
                                          body: RegisterView(
                                        primaryColor: Colors.blue,
                                        backgroundColor: Colors.white,
                                        backgroundImage:
                                            AssetImage("assets/bg.png"),
                                      ))));
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path p = new Path();
    p.lineTo(size.width, 0.0);
    p.lineTo(size.width, size.height * 0.55);
    p.arcToPoint(
      Offset(0.0, size.height * 0.55),
      radius: const Radius.elliptical(50.0, 10.0),
      rotation: 0.0,
    );
    p.lineTo(0.0, 0.0);
    p.close();
    return p;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}
