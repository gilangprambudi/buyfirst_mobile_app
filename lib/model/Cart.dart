import 'dart:convert';
import 'package:buyfirst_mobile/class/api_provider.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

class Cart{
  int id;
  String uid;
  String product;
  String productImage;
  int  price;
  int qty;
  int transactionId;
  int paid;

  Cart.fromJson(Map<String, dynamic> json)
  : id = json['id'],
    uid = json['uid'],
    product = json['product'],
    productImage = json['product_image'],
    price = json['price'],
    qty = json['qty'],
    transactionId = json['transaction_id'],
    paid = json['paid'];

  String getProductImage(){
    return ApiProvider.baseUrl+productImage;
  }

}


class CartHttpService{
  String baseURL = ApiProvider.baseUrl;

  Future getMethod({uri}) async{
    var url = baseURL + uri;
    return http.get(url);
  }

  Future<Cart> getCart() async{
    Cart cart;

    await getMethod(uri: '/cart/id/1').then((response){
      cart = new Cart.fromJson(json.decode(response.body));
    });
    return cart;
  }

  Future<bool> deleteCart() async{

  }


  Future<List<Cart>> getDwelledCartByUID({@required uid}) async{
    List<Cart> carts;

    await getMethod(uri: '/cart/dwell/'+uid).then((response){
      Iterable list = json.decode(response.body);
      carts = list.map((model)=> Cart.fromJson(model)).toList();
    });
    return carts;
    
  }

  Future<int> getDwelledCartTotalPriceByUID(@required uid) async{
    int totalPrice = 0;
    await getMethod(uri: '/cart/dwell/'+uid+'/total-price').then((response){
      totalPrice = int.parse(response.body);
    });
    return totalPrice;
  }

  Future<bool> addCart({@required uid, @required qty}) async{
    print("BOOLLEEAANN");
    await http.post(baseURL + '/cart/add', body: {'uid' : uid, 'qty':1.toString()}).then((response){
      print(response.body.toString());
    });

    return true;
  }


}