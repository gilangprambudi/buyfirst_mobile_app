import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseDataStoreService {
  final databaseReference = Firestore.instance;

  void createRecord({collectionName, recordName, data}) async {
    await databaseReference
        .collection(collectionName)
        .document(recordName)
        .setData(data);
  }

  void updateRecord({collectionName, recordName, data}) {
    try {
      databaseReference
          .collection(collectionName)
          .document(recordName)
          .updateData(data);
    } catch (e) {
      print(e.toString());
    }
  }

  Future<DocumentSnapshot> getCurrentBalance({collectionName, recordName}) async{
    DocumentSnapshot documentSnapshot;
    try{
      await databaseReference.collection('users').document(recordName).get().then((response){
        documentSnapshot = response;
      });
    }catch(e){

    }

    return documentSnapshot;
  }


}
