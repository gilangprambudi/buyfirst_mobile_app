import 'package:buyfirst_mobile/view/login.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: Scaffold(body: LoginView(backgroundColor: 
    Colors.white, backgroundImage: AssetImage("assets/bg.png"), primaryColor: Color(0xFF4aa0d5))));
  }
}
