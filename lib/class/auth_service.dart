import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'firebase_authentication.dart';

class AuthService {

  Future<AuthResult> signIn({email, password}) async {
    AuthResult authResult;
    try{
      FirebaseAuthenticationServiceClass firebaseAuthService =
        new FirebaseAuthenticationServiceClass();
      authResult = await firebaseAuthService.firebaseLogin(email: email, password: password);
    }catch(e){
      authResult = null;
    }

    return authResult;
    
   
  }

  Future<bool> checkSignIn() async {
    bool isSignedIn = false;
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getBool("isLoggedIn") == true) {
      isSignedIn = true;
    }

    return isSignedIn;
  }

  Future<AuthResult> signUp({@required email, @required password}) async{
    AuthResult authResult;
    FirebaseAuthenticationServiceClass firebaseAuthService =
        new FirebaseAuthenticationServiceClass();
    try{
      authResult = await firebaseAuthService.firebaseSignUp(email: email, password: password);
    }catch (e){

    }
    return authResult;
    

  }

  Future<void> sugnInWithGoogle() async {
    FirebaseAuthenticationServiceClass firebaseAuth =
        new FirebaseAuthenticationServiceClass();
    final prefs = await SharedPreferences.getInstance();

    firebaseAuth.firebaseLoginGoogle().then((authResult) {
      prefs.setBool("isLoggedIn", true);
      prefs.setString('savedEmail', authResult.user.email);
      prefs.setString('uid', authResult.user.uid);
    });
  }

  void signOut() async {
    FirebaseAuthenticationServiceClass firebaseAuth =
        new FirebaseAuthenticationServiceClass();
    final prefs = await SharedPreferences.getInstance();

    firebaseAuth.signOut();
    prefs.remove("uid");
    prefs.remove("savedEmail");
    prefs.remove("isLoggedIn");
  }
}
