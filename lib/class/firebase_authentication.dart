import 'package:buyfirst_mobile/class/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class FirebaseAuthenticationServiceClass {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();

  Future<AuthResult> firebaseLogin({email, password}) async{
    AuthResult authResult;
    try{
      authResult = await _auth.signInWithEmailAndPassword(email: email,password: password);
      authResult.user.sendEmailVerification();
    }catch(e){
      print(e.code);
    }
    return authResult;
  }

  Future<AuthResult> firebaseSignUp({email, password}) async {
    AuthResult authResult = await _auth.createUserWithEmailAndPassword(email: email, password: password);
    createFirestoreRecord(authResult);
    return authResult;
  }

  Future<AuthResult> firebaseLoginGoogle() async {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final AuthResult authResult = await _auth.signInWithCredential(credential);
    
    createFirestoreRecord(authResult);
    return authResult;
  }

  void createFirestoreRecord(AuthResult authResult) {
  
    FirebaseDataStoreService firebaseDatastore = new FirebaseDataStoreService();
    firebaseDatastore.createRecord(
        collectionName: 'users',
        recordName: authResult.user.uid,
        data: {'email': authResult.user.email, 'balance': 10000});
  }

  Future<FirebaseUser> getCurrentUser() async {
    FirebaseUser firebaseUser = await _auth.currentUser();
    return firebaseUser;
  }

  

  void signOut() async{
    _auth.signOut();
  }
}


