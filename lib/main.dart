import 'dart:convert';

import 'package:buyfirst_mobile/class/firebase_database.dart';
import 'package:buyfirst_mobile/class/slide_right_route.dart';
import 'package:buyfirst_mobile/model/Cart.dart';
import 'package:buyfirst_mobile/view/CartList.dart';
import 'package:buyfirst_mobile/view/Homemenu.dart';
import 'package:buyfirst_mobile/view/PageViewScreen.dart';
import 'package:buyfirst_mobile/view/PinVerification.dart';
import 'package:buyfirst_mobile/view/Register.dart';
import 'package:buyfirst_mobile/view/login.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'class/auth_service.dart';
import 'class/login_screen.dart';
import 'loader/loader1.dart';
import 'class/auth_class.dart';

//void main() => runApp(MaterialApp(home: LoginView(backgroundColor: Colors.white, primaryColor: Colors.blue, backgroundImage: AssetImage("assets/bg.png"))));
// void getUser() async{
//   FirebaseDataStoreService firestoreSer = new FirebaseDataStoreService();
//   firestoreSer.getCurrentBalance(recordName: 'hvBNhU8kCBgx1txV4usfikAa9Hq1').then((response){
    
//     print(response.data['email']);
//   });
// }
void main() {
 
  //getUser();


  checkFirstTimeRunApp().then((value) {
    if (value == false) {
      AuthService authService = new AuthService();
      authService.checkSignIn().then((isSignedIn) {
        if (isSignedIn) {
          runApp(MaterialApp(home: Scaffold(body: Homemenu())));
        } else {
          runApp(MaterialApp(
              home: Scaffold(
                  body: LoginView(
            primaryColor: Colors.blue,
            backgroundColor: Colors.white,
            backgroundImage: AssetImage("assets/bg.png"),
          ))));
        }
      });
    } else {
      runApp(MaterialApp(home: Scaffold(body: PageViewScreen())));
    }
  });
}

Future<bool> checkFirstTimeRunApp() async {
  final prefs = await SharedPreferences.getInstance();
  print(prefs.getBool('pagerHasBeenSeen') ?? false);
  bool pagerHasBeenSeen = prefs.getBool('pagerHasBeenSeen') ?? false;
  if (!pagerHasBeenSeen) {
    return true;
  } else {
    return false;
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.R
          primarySwatch: Colors.blue,
        ),
        //home: MyHomePage(title: 'Flutter Demo Home Page'),
        //home: MyAppp(),
        home: LoginScreen());
  }
}

class MyApp extends StatefulWidget {
  _MyAppState createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      Navigator.push(context, SlideRightRoute(page: PageViewScreen()));
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return MaterialApp(
        title: 'BuyFirst',
        home: Scaffold(
          body: Stack(
            children: <Widget>[
              Center(
                  child: new Image.asset("assets/bg.png",
                      width: size.width,
                      height: size.height,
                      fit: BoxFit.fill)),
              Container(
                  child: Column(
                children: <Widget>[
                  Align(
                      alignment: Alignment.center,
                      child: Container(
                          margin: EdgeInsets.only(top: 130.0, bottom: 30.0),
                          child:
                              new Image.asset("assets/Logo.png", width: 250))),
                  Align(
                      alignment: Alignment.center,
                      child: (Text(
                        "Your One Stop Payment Service",
                        style: TextStyle(
                            color: Color(0xFFFFFFFF),
                            fontSize: 18,
                            letterSpacing: 2,
                            fontStyle: FontStyle.italic),
                      ))),
                  Align(
                      child: Container(
                          margin: EdgeInsets.only(top: 200),
                          child: new Image.asset("assets/beezhare.png",
                              width: 170),
                          alignment: Alignment.center)),
                  Container(
                      margin: EdgeInsets.only(top: 50.0),
                      child: ColorLoader(
                          colors: [Colors.white],
                          duration: Duration(milliseconds: 1200)))
                ],
              ))
            ],
          ),
        ));
  }
}
